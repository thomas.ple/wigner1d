module solve_schro1d
	IMPLICIT NONE

CONTAINS

	subroutine solve_1D(V,E,Psi,dx,mass)
		real(8), intent(in) :: V(:),dx,mass
		real(8), intent(inout) :: E(:), Psi(:,:)
		integer :: i,j,INFO,n
		real(8) :: SD(size(V)-1), WORK(2*size(V)-2,2*size(V)-2)
		!use atomic units with hbar=1
		!requires LAPACK
		
		n=size(V)
		do i=1,n
			E(i)=1/(mass*dx**2)+V(i)
		enddo
		
		SD(:)=-1/(2*mass*dx**2)

		call dsteqr('I',n,E,SD,Psi,n,WORK,INFO)		
		if(INFO /= 0) then
			write(0,*) "ERREUR dsteqr:",INFO
			stop
		end if
		
	end subroutine solve_1D
	
end module solve_schro1d