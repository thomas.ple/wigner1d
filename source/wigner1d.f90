PROGRAM wigner1d
	use atomic_units
	use param
	use potentials, ONLY: Pot,read_pot_nml,assign_Pot
	use solve_schro1d
	IMPLICIT NONE	
	character(200) :: nmlfile
	character(10) :: flags
	character(1) :: solve_decide
	real(8) :: dx,dp
	real(8), allocatable :: x(:),p(:),V(:),Psi(:,:),Ener(:)
	real(8), allocatable :: margX(:),WORK(:),margP(:),rft(:),reWig(:,:)
	real(8), allocatable :: Wig(:,:)
	real(8) :: partition,norm,E0, Wig_tmp
	real(8), allocatable :: stat_fac(:),omega(:),theta(:)
	real(8), allocatable :: spec(:), spectrum(:)
	real(8) :: x1nm,omnm
	integer :: i,j,k,nconv,ifail
	real :: timeStart,timeFinish
	logical :: file_exists
	real(8) :: Funit
	integer :: plim

	!READ NAMELIST (namelist file must be passed in 1st argument)
	call get_command_argument(1,nmlfile)
	open(1,file=trim(nmlfile))
		read(1,nml=PARAMETERS)
	close(1)
	open(1,file=trim(nmlfile))
		read(1,nml=GRID_PARAMETERS)
	close(1)
	open(1,file=trim(nmlfile))
		read(1,nml=SPECTRUM_PARAMETERS)
	close(1)
	
	!CONVERT IN A.U.
		mass=mass*Mprot
		T=kb*Temperature/kelvin

		xmin=xmin/bohr
		xmax=xmax/bohr
		pmax=pmax*Mprot*fs/bohr

		if(in_THz) then
			Funit=THz/(2*pi)
		else
			Funit=cm1
		endif
		damp=damp/Funit
		OmMax=OmMax/Funit
		dOm=dOm/Funit
		
	! READ POTENTIAL NAMELIST
	call read_pot_nml(mass,nmlfile,1)
	call assign_Pot(pot_name,1)
	
	!GET FLAGS
	call get_command_argument(2,flags)
	if(flags(1:1)=="-") then
		if(index(flags,'r')>-1 .OR. index(flags,'R')>-1) then
			retrieve_Psi=.True.
			solve_schro=.False.
		endif
	endif

	call system("mkdir "//trim(outdir))

	call cpu_time(timeStart)

!-----------------------------------------------	
	!STORE DISCRETIZATION POINTS
	dx=(xmax-xmin)/(xstep-1)
	write(*,*) "dx=",dx*bohr,"Angstrom"
	allocate(x(xstep),V(xstep))
	x(:)=(/ (xmin+(i-1)*dx, i = 1,xstep) /)
	!WRITE POTENTIAL
	open(10,file=trim(outdir)//"/potential.out")
	do i=1,xstep
		V(i)=Pot(x(i))
		write(10,*) x(i)*bohr,V(i)
	enddo
	close(10)

!-----------------------------------------------
	!SOLVE SCHRODINGER
	allocate(Psi(xstep,xstep),Ener(xstep))
	Psi=0 ; Ener=0
	call solve_1D(V,Ener,Psi,dx,mass)

	!COMPUTE PARTITION FUNCTION AND THERMAL FACTORS
	partition=sum(exp(-Ener(1:nev)/T))
	nconv=nev
	do k=1,nev
		if (exp(-Ener(k)/T)/partition<1e-10) then
			nconv=k
			exit
		endif	
	enddo
	write(*,*) "last eigen-vector considered :", nconv
	allocate(stat_fac(nconv))
	stat_fac(1:nconv)=exp(-Ener(1:nconv)/T)/partition

	!WRITE ENERGIES AND PSIs
	open(10,file=trim(outdir)//"/Energies.out")
	E0=Ener(1)
	do k=1,nconv
		Ener(k)=Ener(k)-E0
		write(10,*) k,Ener(k),stat_fac(k)
	enddo
	close(10)
	
	open(10,file=trim(outdir)//"/Psi.out")
	do i=1,xstep
		write(10,*) x(i)*bohr,Psi(i,1:nconv)
	enddo
	close(10)
	
!-----------------------------------------------
	!COMPUTE WIGNER FUNCTION
	dp=2*pmax/(pstep-1)
	allocate(p(pstep))
	p(:)=(/ (-pmax+(i-1)*dp, i = 1,pstep) /)
	allocate(Wig(xstep,pstep))
	Wig=0
	do i=1,xstep
		Wig_tmp=sum(stat_fac(1:nconv)*Psi(i,1:nconv)**2)/2
		Wig(i,:)=Wig(i,:)+Wig_tmp
		do k=1,i
			if(i-k>0 .and. i+k<=xstep) then
				Wig_tmp=sum(stat_fac(1:nconv)*Psi(i-k,1:nconv)*Psi(i+k,1:nconv))
				do j=1,pstep
					Wig(i,j)=Wig(i,j)+cos(p(j)*(2*k)*dx)*Wig_tmp
				enddo
			endif
		enddo
	enddo

	write(*,*) "FFT OK"

	!NORMALIZE AND COMPUTE MARGINALS
	allocate(margX(xstep),margP(pstep))
	Wig=Wig/(sum(Wig)*dx*bohr*dp*bohr/(Mprot*fs))
	margX=0 ; margP=0
	do i=1,xstep
		margX(i)=sum(Wig(i,:))*dp*bohr/(Mprot*fs)
	enddo
	do i=1,pstep
		margP(i)=sum(Wig(:,i))*dx*bohr
	enddo	

	write(*,*) "Marginals OK"

	open(10,file=trim(outdir)//"/Wigner.out")
	do i=1,xstep ; do j=1,pstep
		write(10,*) x(i)*bohr,p(j)*bohr/(Mprot*fs),Wig(i,j)
	enddo ; write(10,*) ; enddo
	close(10)

	open(10,file=trim(outdir)//"/Wigner_margX.out")
	do i=1,xstep
		write(10,*) x(i)*bohr,margX(i)
	enddo
	close(10)
	open(10,file=trim(outdir)//"/Wigner_margP.out")
	do i=1,pstep
		write(10,*) p(i)*bohr/(Mprot*fs),margP(i)
	enddo
	close(10)

!-----------------------------------------------
	!COMPUTE SPECTRUM
	Fstep=int(OmMax/dOm)+1
	allocate(omega(1:Fstep),theta(1:Fstep))
	allocate(spec(1:Fstep),spectrum(1:Fstep))
	spectrum=0
	omega(:)=(/ ((i-1)*dOm, i=1,Fstep) /)
	theta(1)=0
	theta(2:Fstep)=omega(2:Fstep)*( 0.5 + 1/(exp( omega(2:Fstep)/T)-1) )
	do i = 1,nconv-1 ; do j = i+1, nconv
		omnm=Ener(j)-Ener(i)
		x1nm=sum(x(:)*psi(:,i)*psi(:,j))
		spec(:)=4*theta(:)*(stat_fac(i)-stat_fac(j))*damp &
			*(omnm/(damp**2+(omnm-omega(:))**2)+omnm/(damp**2+(omnm+omega(:))**2))
		spectrum(:)=spectrum(:)+spec(:)*abs(x1nm)**2
	enddo ; enddo

	spectrum=spectrum/(sum(spectrum)*dOm*Funit)
	open(10,file=trim(outdir)//"/spectrum.out")
	if(in_THz) then
		write(10,*) "# Frequency unit: THz"
	else
		write(10,*) "# Omega unit: cm^-1"
	endif
	do i=1,Fstep
		write(10,*) omega(i)*Funit,spectrum(i)
	enddo
	close(10)
	write(*,*) "Spectrum OK"

!-----------------------------------------------
	!END PROGRAM
	call cpu_time(timeFinish)
	write(*,*) "Job done in",timeFinish-timeStart,"s."


CONTAINS

END PROGRAM wigner1d
