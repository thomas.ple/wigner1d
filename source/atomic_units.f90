module atomic_units
	implicit none
	real(8), parameter :: eV     = 27.211_8       ! Hartree to eV
	real(8), parameter :: kcalperMol     = 627.509_8       ! Hartree to kcal/mol
	real(8), parameter :: bohr   = 0.5297721_8    ! Bohr to Angstrom
	real(8), parameter :: Mprot    = 1836.15        ! proton mass
!	real(8), parameter :: hbar   = 1._8           ! Planck's constant
	real(8), parameter :: fs     = 2.4188843e-2_8 ! AU time to femtoseconds
	real(8), parameter :: kelvin = 3.15774e5_8    ! Hartree to Kelvin
	real(8), parameter :: THz = 1000/fs		! AU frequency to THz
	real(8), parameter :: nNewton = 82.387_8		! AU Force to nNewton 
	real(8), parameter :: cm1=219471.52_8  ! Hartree to cm-1 
end module atomic_units

